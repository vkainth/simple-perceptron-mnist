import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
import numpy as np
import tkinter as tk
import Kainth_02_04 as k04
from sklearn.model_selection import train_test_split
import pandas as pd


class DisplayActivationFunctions:
    """
    This class is for displaying activation functions for NN.
    Farhad Kamangar 2017_08_26
    """

    def __init__(self, root, master, *args, **kwargs):
        self.master = master
        self.root = root
        #########################################################################
        #  Set up the constants and default values
        #########################################################################
        self.xmin = 1
        self.xmax = 1000
        self.ymin = 0
        self.ymax = 100
        self.alpha = 0.1
        self.iters = 0
        # Data import
        self.inp_vec = k04.input_data("Data/*.png")
        # self.input_vec = self.inp_vec[:, 784]
        self.training_var = self.inp_vec[:, :784]
        self.training_classes = self.inp_vec[:, 784:]
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.training_var,
                                                                                self.training_classes,
                                                                                test_size=0.2)
        print("self.X_test.shape: ", self.X_test.shape)
        # DEBUG
        # print("training_var: ", training_var)
        # print("self.training_classes: ", self.training_classes)
        print("self.X_train.shape: ", self.X_train.shape)
        # Initial activation function is hardlims
        self.activation_function = "Symmetrical Hard Limit"
        self.learning_method = "Filtered Learning"
        # self.weights = np.ones((self.y_train.shape[1], self.X_train.shape[1]))
        # self.weights.reshape((10, 784))
        self.weights = np.dot(self.y_train.T, self.X_train)
        print("self.weights.shape: ", self.weights.shape)
        self.error = 100.0
        self.errors = {}
        self.errors[0] = 100.0
        #########################################################################
        #  Set up the plotting area
        #########################################################################
        self.plot_frame = tk.Frame(self.master)
        self.plot_frame.grid(row=0, column=0, columnspan=2, sticky=tk.N + tk.E + tk.S + tk.W)
        self.plot_frame.rowconfigure(0, weight=1)
        self.plot_frame.columnconfigure(0, weight=1)
        self.figure = plt.figure("")
        self.axes = self.figure.gca()
        self.axes.set_xlabel('Number of Epochs')
        self.axes.set_ylabel('Error Rate (%)')
        self.axes.set_title("")

        plt.xlim(self.xmin, self.xmax)
        plt.ylim(self.ymin, self.ymax)
        self.canvas = FigureCanvasTkAgg(self.figure, master=self.plot_frame)

        self.plot_widget = self.canvas.get_tk_widget()
        self.plot_widget.grid(row=0, column=0, sticky=tk.N + tk.E + tk.S + tk.W)
        #########################################################################
        #  Set up the frame for sliders (scales)
        #########################################################################
        self.sliders_frame = tk.Frame(self.master)
        self.sliders_frame.grid(row=1, column=0, sticky=tk.N + tk.E + tk.S + tk.W)
        self.sliders_frame.rowconfigure(0, weight=10)
        # set up the sliders
        self.alpha_slider = tk.Scale(self.sliders_frame, variable=tk.DoubleVar(), orient=tk.HORIZONTAL,
                                     from_=0.001, to_=1.0, resolution=0.001, bg="#DDDDDD",
                                     activebackground="#FF0000",
                                     highlightcolor="#00FFFF",
                                     label="Alpha",
                                     command=lambda event: self.alpha_slider_callback())
        self.alpha_slider.set(self.alpha)
        self.alpha_slider.bind("<ButtonRelease-1>", lambda event: self.alpha_slider_callback())
        self.alpha_slider.grid(row=0, column=0, sticky=tk.N + tk.E + tk.S + tk.W)
        #########################################################################
        #  Set up the frame for button(s)
        #########################################################################

        self.buttons_frame = tk.Frame(self.master)
        self.buttons_frame.grid(row=1, column=1, sticky=tk.N + tk.E + tk.S + tk.W)
        self.buttons_frame.rowconfigure(0, weight=1)
        self.buttons_frame.rowconfigure(1, weight=2)
        self.buttons_frame.columnconfigure(0, weight=1, uniform='xx')
        self.buttons_frame.columnconfigure(1, weight=2, uniform='xx')
        self.label_for_activation_function = tk.Label(self.buttons_frame, text="Transfer Functions",
                                                      justify="center")
        self.label_for_activation_function.grid(row=0, column=1, sticky=tk.N + tk.E + tk.S + tk.W)

        self.activation_function_variable = tk.StringVar()
        self.activation_function_dropdown = tk.OptionMenu(self.buttons_frame, self.activation_function_variable,
                                                          "Symmetrical Hard Limit", "Linear", "Hyperbolic Tangent",
                                                          command=lambda event: self.activation_function_dropdown_callback())
        self.activation_function_variable.set(self.activation_function)
        self.activation_function_dropdown.grid(row=2, column=1, sticky=tk.N + tk.E + tk.S + tk.W)

        self.learning_method_variable = tk.StringVar()
        self.learning_method_dropdown = tk.OptionMenu(self.buttons_frame, self.learning_method_variable,
                                                      "Filtered Learning", "Delta Rule", "Unsupervised Hebb",
                                                      command=lambda
                                                      event: self.learning_method_dropdown_callback())
        self.learning_method_variable.set(self.learning_method)
        self.learning_method_dropdown.grid(row=3, column=1, sticky=tk.N + tk.E + tk.S + tk.W)

        self.train_button = tk.Button(self.buttons_frame, text="Adjust Weights (Train)", command=self.train_button_callback)
        self.train_button.grid(row=1, column=1, sticky=tk.N + tk.E + tk.S + tk.W)
        self.random_data_button = tk.Button(self.buttons_frame, text="Randomize Weights", command=self.randomize_wts)
        self.random_data_button.grid(row=0, column=1, sticky=tk.N + tk.E + tk.S + tk.W)
        self.randomize_wts()
        self.display_activation_function()
        print("Window size:", self.master.winfo_width(), self.master.winfo_height())

    def display_activation_function(self):
        """
        Plots the decision boundary and correctly labels each side
        :return: None
        """
        x_pts = list()
        y_pts = list()
        for idx, err in self.errors.items():
            x_pts.append(idx)
            y_pts.append(np.max(err))
        print("x_pts: ", x_pts)
        print("y_pts: ", y_pts)
        self.axes.cla()
        self.axes.cla()
        self.axes.plot(x_pts, y_pts)
        plt.xlim(self.xmin, self.xmax)
        plt.ylim(self.ymin, self.ymax)
        self.axes.set_xlabel('Epochs')
        self.axes.set_ylabel('Error Rate')
        self.axes.set_title("Assignment 02 - Kainth")
        # self.axes.legend(loc='upper right')
        self.canvas.draw()

    def perceptron_predict(self, x_val):
        """
        Predicts the activation of the current row value
        :param x_val: current row value
        :return: expression depending upon activation function
        """
        if self.activation_function == 'Symmetrical Hard Limit':
            return np.where(self.cls_net_value(x_val) >= 0.0, 1, -1)
        if self.activation_function == 'Linear':
            return self.cls_net_value(x_val)
        if self.activation_function == 'Hyperbolic Tangent':
            return (np.exp(self.cls_net_value(x_val)) - np.exp(-self.cls_net_value(x_val))) \
                / (np.exp(self.cls_net_value(x_val)) + np.exp(-self.cls_net_value(x_val)))

    def cls_net_value(self, x_val):
        """
        Calculates net value for current row
        :param x_val: current row value
        :return: Addition of bias and dot product of weights and row value
        """
        return np.dot(self.weights, x_val.T)

    def train_button_callback(self):
        """
        Trains the simple perceptron against the selected activation function
        :return: None
        """
        # self.weights = np.dot(self.training_classes.T, self.training_var)
        # DEBUG
        # print("training_classes: ", training_classes)
        for i in range(1, 101):
            self.iters += 1
            self.display_activation_function()
            self.error = 0.0
            # for row, tr_val in zip(self.X_train, self.y_train):
            #     # print("BEING ZIPPED!")
            #     row_transpose = np.asarray(row).reshape((1, 784))
            #     t_transpose = np.asarray(tr_val).reshape((10, 1))
            #     # print("t_transpose.shape: ", t_transpose.shape)
            #     activation = self.perceptron_predict(x_val=row_transpose.astype(int))
            #     # print("activation: ", activation)
            #     # print("activation.shape: ", activation.shape)
            #     activation = np.asarray(activation).reshape((10, 1))
            #     if self.learning_method == "Delta Rule":
            #         # print("IN DELTA RULE")
            #         update_val = np.dot(t_transpose - activation, row_transpose)
            #         # print("pd.DataFrame(row_transpose).head(): ", pd.DataFrame(row_transpose).head())
            #         # print("activation: ", activation)
            #         self.weights += self.alpha * update_val
            #     if self.learning_method == "Filtered Learning":
            #         # print("IN FILTERED LEARNING")
            #         update_val = np.dot(t_transpose, row_transpose)
            #         self.weights = ((1 - self.alpha) * self.weights) + (self.alpha * update_val)
            #     if self.learning_method == "Unsupervised Hebb":
            #         # print("IN UNSUPERVISED HEBB")
            #         update_val = self.alpha * np.dot(activation, row_transpose)
            #         self.weights += update_val
            # for row, tr_val in zip(self.X_train, self.y_train):
            # print("BEING ZIPPED!")
            row_transpose = np.asarray(self.X_train).reshape((800, 784))
            t_transpose = np.asarray(self.y_train).reshape((10, 800))
            # print("t_transpose.shape: ", t_transpose.shape)
            activation = self.perceptron_predict(x_val=row_transpose.astype(int))
            # print("activation: ", activation)
            # print("activation.shape: ", activation.shape)
            activation = np.asarray(activation).reshape((10, 800))
            if self.learning_method == "Delta Rule":
                # print("IN DELTA RULE")
                update_val = np.dot(t_transpose - activation, row_transpose)
                # print("pd.DataFrame(row_transpose).head(): ", pd.DataFrame(row_transpose).head())
                # print("activation: ", activation)
                self.weights += self.alpha * update_val
            if self.learning_method == "Filtered Learning":
                # print("IN FILTERED LEARNING")
                update_val = np.dot(t_transpose, row_transpose)
                self.weights = ((1 - self.alpha) * self.weights) + (self.alpha * update_val)
            if self.learning_method == "Unsupervised Hebb":
                # print("IN UNSUPERVISED HEBB")
                update_val = self.alpha * np.dot(activation, row_transpose)
                self.weights += update_val
            test_activation = self.cls_net_value(x_val=self.X_test)
            # print("test_activation.shape: ", test_activation.shape)
            test_activation = test_activation.reshape((200, 10))
            err_idxs = np.argmax(test_activation, axis=1)
            tr_idxs = np.argmax(self.y_test, axis=1)
            # print("err_idxs: ", err_idxs)
            # print("tr_idxs: ", tr_idxs)
            # cnt = 0
            # print("self.y_test.shape: ", self.y_test.shape)
            # for idx in range(len(self.y_test)):
            #     if test_activation[i] == self.y_test[i]:
            #         cnt += 1
            cnt_nonzero = np.count_nonzero(tr_idxs - err_idxs)
            print("cnt_nonzero: ", cnt_nonzero)
            # print("tr_idxs - err_idxs: ", tr_idxs - err_idxs)
            # print("cnt_nonzero: ", cnt_nonzero)
            cnt_zero = len(tr_idxs) - cnt_nonzero
            print("len(tr_idxs): ", len(tr_idxs))
            print("cnt_zero: ", cnt_zero)
            # print("cnt_zero: ", cnt_zero)
            self.error = (cnt_nonzero) / (cnt_zero + cnt_nonzero)
            # print("self.error: ", self.error)
            self.errors[self.iters] = self.error * 100
            # print("i: ", i)
            # print("self.iters: ", self.iters)

        # self.clear_values()

    def randomize_wts(self):
        """
        Creates a 784x1 vector of randomized weights in the range -0.001 and 0.001
        :return: None
        """
        self.clear_values()
        self.weights = np.random.uniform(low=-0.001, high=0.001,
                                         size=(self.training_classes.shape[1], self.training_var.shape[1]))
        self.weights.reshape((10, 784))
        self.display_activation_function()

    def clear_values(self):
        """
        Resets weights and alpha to defaults
        :return: None
        """
        self.iters = 0
        self.errors = {}
        self.error = 100.0
        self.alpha = 0.1
        self.weights = np.dot(self.y_train.T, self.X_train)
        self.weights.reshape((10, 784))
        self.display_activation_function()

    def alpha_slider_callback(self):
        self.alpha = self.alpha_slider.get()
        self.display_activation_function()

    def activation_function_dropdown_callback(self):
        self.activation_function = self.activation_function_variable.get()
        self.display_activation_function()

    def learning_method_dropdown_callback(self):
        self.learning_method = self.learning_method_variable.get()
        self.display_activation_function()
