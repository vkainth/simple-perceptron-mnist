# Simple Perceptron With MNIST Data
This project builds on the [simple-perceptron](https://github.com/vibhor-kainth/simple-perceptron)
repository as a submission for CSE 5368 - Neural Networks. The simple perceptron is a single-layer
neural network capable of separating linearly-separable data points. 
This implementation uses 10 nodes.

## GUI Description
* One slider: for the learning rate (alpha)
* Two buttons: one to train the perceptron for 100 epochs and the other to randomize the weights        between -0.0001 and 0.0001
* Learning Method Drop-down: select between three Hebbian rules -> Filtered Learning, Delta Rule, and Unsupervised Hebb
* Transfer Function Drop-down: select between three transfer functions -> Symmetrical Hard Limit, Hyperbolic Tangent, and Linear

## Data Description
Set of 1000 images, each of 28x28 pixels, representing a digit in the range 0 to 9.

## Screenshots
![Application Start](01.png?raw=true "Application Start")
![Train Perceptron](02.png?raw=true "Train Perceptron")