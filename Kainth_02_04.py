import numpy as np
import scipy.misc
import glob
import pandas as pd

""" --> Based on code from Tariq Rashid's IPython file
    --> at https://github.com/makeyourownneuralnetwork/makeyourownneuralnetwork/blob/master/part3_load_own_images.ipynb
"""


def input_data(data_path):
    """
    Converts a directory of images to vector of numbers with first column as label
    :param data_path: path of directory of data images
    :return: numpy array of 1000 rows and 785 columns
    """
    inp_list = list()
    for img_file in glob.glob(data_path):
        print("loading... ", img_file)
        label = img_file[-11:-10] + "_"
        # print("Label: ", label)
        img_arr = scipy.misc.imread(img_file, flatten=True)
        # img_data = 255.0 - img_arr.reshape(784)
        img_data = img_arr.reshape(784)
        # scale data between -1 and 1
        img_data = (img_data / 127.5) - 1
        print("Min: ", np.min(img_data))
        print("Max: ", np.max(img_data))
        # append label and image data to dataset
        record = np.append(label, img_data)
        # print("Record: ")
        # print(record)
        inp_list.append(record)
    inp_df = pd.DataFrame(inp_list)
    inp_df = inp_df.apply(pd.to_numeric, errors='ignore')
    inp_arr = pd.get_dummies(inp_df, prefix='Img')
    print("inp_arr.head().values: ", inp_arr.head().values)

    return inp_arr.values


if __name__ == '__main__':
    input_data("Data/*.png")
